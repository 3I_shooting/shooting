﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LiveEnemy : MonoBehaviour {
    private int state = 0;  //0=目標への移動,1カメラに向き射撃
    public GameObject bullet;
    public GameObject shooter;
    bool shotFlag = false;
    bool shot = false;
    int shotCount;
    public GameObject target;
    NavMeshAgent agent;
    Animator animator;

    // Use this for initialization
    void Start() {
        animator = GetComponent<Animator>();

        //navemeshセットアップ
        agent = GetComponent<NavMeshAgent>();
        agent.speed = 3.5f;
        agent.destination = target.transform.position;
        agent.updateRotation = false;

        animator.SetBool("run", true);
    }

    // Update is called once per frame
    void Update() {
        if(Random.Range(0,100)==0 && !shotFlag && state==1){
            shotFlag = true;
            shot = true;
            shotCount = 0;
        }

        if (shot) {
            shot = false;
            if (++shotCount != 5) {
                Invoke("shotSwitch",0.2f);
            }
            else {
                shotFlag = false;
            }
            Vector3 gunPosi = shooter.transform.position;
            GameObject shoted=Instantiate(bullet, gunPosi, new Quaternion(0, 0, 0, 0));  //弾を生成し、生成した弾のobj取得
            bullet b=shoted.GetComponent<bullet>();     //スクリプトオブジェクト取得
            if (Random.Range(0,11) == 0) {
                b.setAim(false);             //プレイヤーを外す指示
            }
            else {
                b.setAim(true);            //プレーヤーを狙う指示
            }

        }

        if (Vector3.Distance(target.transform.position, this.transform.position) <=1f && state==0) {
            agent.velocity = Vector3.zero;
            agent.Stop();
            agent.enabled = false;
            state = 1;
            animator.SetBool("run", false);
        }
        //移動中
        if (state == 0) {
            look(agent.steeringTarget); //進行方向を見る
        }
        //移動後
        if (state == 1) {
            look(Camera.main.transform.position);   //プレイヤーを見る
        }

    }

    //引数の位置に敵を向かせる
    void look(Vector3 lookPosition) {
        var newRotation = Quaternion.LookRotation(lookPosition - transform.position).eulerAngles;
        newRotation.x = 0;      //y軸以外は動かさないため
        newRotation.y+= 30f;   //これくらいずらすと銃がうまく向く
        newRotation.z = 0;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(newRotation), Time.deltaTime * 3.5f);
    }

    void shotSwitch() {
        shot = true;
    }
    void OnAnimatorMove() {
        transform.position = animator.rootPosition;
    }

}
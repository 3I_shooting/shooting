﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    public GameObject enemy;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
        if (Random.Range(0, 140) == 0) {
            Instantiate(enemy,new Vector3(Random.Range(3f,10f),10f,0), new Quaternion(0,180f,0,0));
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void delete() {
        Destroy(gameObject);
    }

    public void setAim(bool Aim) {
        Vector3 random;
        if (Aim) {
            random = new Vector3(
                Random.Range(1f, 3.5f) * Random.Range(0, 1f) * 2f - 1f,
                Random.Range(1f, 3.5f) * Random.Range(0, 1f) * 2f - 1f,
                Random.Range(1f, 3.5f) * Random.Range(0, 1f) * 2f - 1f
                );
        }
        else random = new Vector3(0, 0, 0);

        Rigidbody rg = GetComponent<Rigidbody>();
        Vector3 forward = Camera.main.transform.position-random  - this.transform.position; //銃からカメラへのベクトル
        forward = forward.normalized;                           //単位ベクトル化
        rg.AddForce(forward * 10f, ForceMode.VelocityChange);
        Invoke("delete", 10f);
    }
}

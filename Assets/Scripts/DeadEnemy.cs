﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadEnemy : MonoBehaviour {

    // Use this for initialization
    void Start () {
        Invoke("delete", 3f);                   //3秒後にオブジェクト消去
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int forceObj(GameObject hit) {
        string name = hit.name;
        //Debug.Log(name);

        if (name == "Bip001 L Thigh") name = "Bip001/Bip001 Pelvis/Bip001 L Thigh";
        else if (name == "Bip001 L Calf") name = "Bip001/Bip001 Pelvis/Bip001 L Thigh/Bip001 L Calf";
        else if (name == "Bip001 R Thigh") name = "Bip001/Bip001 Pelvis/Bip001 R Thigh";
        else if (name == "Bip001 R Calf") name = "Bip001/Bip001 Pelvis/Bip001 R Thigh/Bip001 R Calf";
        else if (name == "Bip001 Spine") name = "Bip001/Bip001 Pelvis/Bip001 Spine";
        else if (name == "Bip001 L UpperArm") name = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001 L UpperArm";
        else if (name == "Bip001 L Forearm") name = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001 L UpperArm/Bip001 L Forearm";
        else if (name == "Bip001 R UpperArm") name = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001 R UpperArm";
        else if (name == "Bip001 R Forearm") name = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001 R UpperArm/Bip001 R Forearm";
        else if (name == "Bip001 Head") name = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head";

        Rigidbody rg = this.transform.Find(name).gameObject.GetComponent<Rigidbody>();  //hitしたobject名と同じﾗｸﾞﾄﾞｰﾙobjectのrg取得
        Vector3 forward;                    //カメラから敵へのベクトル格納変数

        forward = this.transform.position - Camera.main.transform.position;   //カメラから敵へのベクトル
        forward = forward.normalized;                               //単位ベクトル化
        rg.AddForce(forward * Random.Range(1500, 2200));                                  //撃たれた方向の逆にふっとぶ

        return 0;
    }

    void delete() {
        GameObject parent;
        parent = this.transform.parent.gameObject;
        Destroy(parent);
    }

}

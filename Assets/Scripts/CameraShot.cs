﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShot : MonoBehaviour {
    public Texture2D cur;
    public bool action = false;

    GameObject parent;
    Vector3 firstPos;

    int[] eNum = new int[2] { 5,4};
    int[] time = new int[2] { 6, 7 };

    int rootCount = 0;
    int killCount = 0;

	// Use this for initialization
	void Start () {

        Cursor.SetCursor(cur,new Vector2(cur.width / 2, cur.height / 2), CursorMode.ForceSoftware);
        parent = transform.parent.gameObject;
        rootSet();
        
    }

    // Update is called once per frame
    void Update () {
        
        GameObject obj = getClickObject();  //hitしたオブジェクトを取得
        GameObject liveObj;
        GameObject deadObj;                 //ラグドールのオブジェクト 
        Vector3 livePosi;
        Quaternion liveRote;
        

        //スペースキー入力の視点移動
        if (!Input.GetKey(KeyCode.Space) && this.transform.position.y > firstPos.y-0.8f && action) {
            Vector3 tmp = transform.position;
            tmp.y -= 0.1f;
            this.transform.position = tmp;
        }
        else if (Input.GetKey(KeyCode.Space) && this.transform.position.y < firstPos.y && action) {
            Vector3 tmp = transform.position;
            tmp.y += 0.1f;
            this.transform.position = tmp;
        }

        if (obj != null)// 以下オブジェクトがクリックされた時の処理
        {
            if (obj.tag == "Enemy") {   //敵なら死体に切り替える
                liveObj = obj;
                do {
                    liveObj = liveObj.transform.parent.gameObject;  //当てたところから辿って親を取得
                } while (liveObj.name!="Enemy_live");
   
                //liveObjの兄弟オブジェクトのラグドール取得
                deadObj = liveObj.transform.parent.gameObject.transform.Find("Enemy_dead").gameObject;

                livePosi = liveObj.transform.position;                      //撃たれた時の座標保存
                liveRote = liveObj.transform.rotation;

                deadObj.SetActive(true);                                    //ラグドール有効
                deadObj.transform.position = livePosi;                      //死体を撃たれた時の座標に
                deadObj.transform.rotation = liveRote;

                DeadEnemy d = deadObj.GetComponent<DeadEnemy>();
                d.forceObj(obj);

                Destroy(liveObj);                               //生きてたオブジェクトは消去
                killCount++;
                Debug.Log("kill is "+killCount);
            }
        }

        if (killCount == eNum[rootCount-1]) {
            killCount = 0;
            action = false;
            rootSet();
        }
    }

    // 左クリックしたオブジェクトを取得する関数(3D)
    public GameObject getClickObject()
    {
        GameObject result = null;
       

        // 左クリックされた場所のオブジェクトを取得
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();
            if (Physics.SphereCast(ray,0.1f, out hit))
            {
                result = hit.collider.gameObject;
            }
        }
        return result;
    }


    void rootEnd() {
        action = true;
        firstPos = this.transform.position;
        Debug.Log("action!");
    }

    void rootSet() {
        Hashtable hash = new Hashtable();
        //Vector3[] _paths;
        //_paths = new Vector3[] {
        //    new Vector3(23.97425f, 2.738f, 21f),
        //    new Vector3(24.84266f, -0.9025018f, 24.90361f),
        //    new Vector3(25.43f, -1.496f, 28.744f),
        //};

        Vector3[][] _paths = new Vector3[][] {
            new Vector3[] {
                new Vector3(23.97425f, 2.738f, 21f),
                new Vector3(24.84266f, -0.9025018f, 24.90361f),
                new Vector3(25.43f, -1f, 28.744f),
            },
            new Vector3[] {
                new Vector3(25.43f,-1f, 28.744f),
                new Vector3(24.84394f, -0.3961711f, 38.19025f),
                new Vector3(35.1019f, -1f, 37.31392f),
            }
        };

        //string pathName = "root" + (rootCount+1);
        string waveName = "wave" + (rootCount+1);

        // プレハブを取得
        GameObject prefab = (GameObject)Resources.Load(waveName);
        // プレハブからインスタンスを生成
        Instantiate(prefab, new Vector3(0,0,0), Quaternion.identity);

        //hash.Add("path", iTweenPath.GetPath(pathName));   //連続だと使えない
        hash.Add("path", _paths[rootCount]);
        hash.Add("time", time[rootCount]);
        hash.Add("easeType", iTween.EaseType.easeInOutQuad);
        hash.Add("orienttopath", true);
        hash.Add("oncomplete", "rootEnd");
        hash.Add("oncompletetarget", this.gameObject);

        // iTween.RotateTo(this.gameObject, iTween.Hash("y", 11,"islocal",true,"time",6));
        iTween.MoveTo(parent, hash);

        rootCount++;
    }
}
